#!/usr/bin/python3
from flask import Flask,request
import traceback

app = Flask(__name__)

class state:
    pass

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/reset")
def reset():

    # direction du deplacement en cours -> +1, -1, 0
    state.direction = 0

    # etage courant
    state.level = 0

    # portes ouvertes
    state.door_open = False

    # dernier etage desservi
    state.last_level = -1

    # appels
    state.calls_up   = [False] * 6
    state.calls_down = [False] * 6

    # nombre de personnes dans l'ascenseur
    state.nb_personnes = 0

    return "OK"

@app.route("/call", methods=['GET'])
def call():
    atFloor = int(request.args.get('atFloor'))
    to = request.args.get('to')
    print ("call received from %d to %s" % (atFloor, to))
    
    calls = state.calls_up if to == "UP" else state.calls_down
    calls[atFloor] = True

    print ("-> calls %s %r" % (to, calls))

    return ("")

@app.route("/go")
def go():
    try:
        toFloor = int(request.args.get('floorToGo'))

        print ("go to %d" % toFloor)

        calls = state.calls_up if toFloor > state.level else state.calls_down
        calls[toFloor] = True

        print ("-> calls %r" % (calls))
        return ("")
    except:
        traceback.print_exc()
        raise

@app.route("/userHasEntered")
def has_entered():
    print ("user has entered")
    return ""

@app.route("/userHasExited")
def has_exited():
    print ("user has exited")
    return ""

def nextCommand():

    if state.direction == 0:
        state.direction = 1

    def move():
        state.level += state.direction
        if state.direction > 0:
            return "UP"
        elif state.direction < 0:
            return "DOWN"
        else:
            return "NOTHING"

    if state.direction > 0:
        calls = state.calls_up
    else:
        calls = state.calls_down


    if state.door_open:
        state.door_open = False
        calls[state.level] = False
        return "CLOSE"
    else:
        if calls[state.level]:
            state.door_open = True
            return "OPEN"
        elif state.direction > 0:
            if state.level == 5:
                state.direction = -1
                return nextCommand()
            return move()
        elif state.direction < 0:
            if state.level == 0:
                state.direction = 1
                return nextCommand()
            return move()
           
    return "NOTHING"

@app.route("/nextCommand")
def nxt():
    try:
        cmd = nextCommand()
        print (" -> %s", cmd)
        return cmd
    except:
        traceback.print_exc()
        raise

reset()

if __name__ == "__main__":
    app.run(port=4000)

